const http = require('http')
const express = require('express');
const { __express } = require('ejs');
const app = express()
const port = 3001
app.set('view engine', 'ejs');
app.use(express.static(__dirname + '/public'))
const bodyParser = require("body-parser")
app.use(bodyParser.urlencoded({extenden:true}))

let datos =[{
    matrucula:"2020030456",
    nombre:"Acosta Ortega Jesus Humberto",
    sexo:"M",
    materias:["Ingles","Tecnologias de internet","Base de datos"]
},
{
        matrucula:"2020030678",
        nombre:"Acosta Valeria Irving Guadalupe",
        sexo:"F",
        materias:["Ingles","Tecnologias de internet","Base de datos"]
},
{
    matrucula:"2020030299",
    nombre:"Quintero Sanchez Cristian Genjiro",
    sexo:"M",
    materias:["Ingles","Tecnologias de internet","Base de datos"]
},
{
    matrucula:"2020030240",
    nombre:"Martinez Valdes Saul Eduardo",
    sexo:"M",
    materias:["Ingles","Tecnologias de internet","Base de datos"]
},
{
    matrucula:"2020030810",
    nombre:"Garcia Medrano Carlos Alberto",
    sexo:"M",
    materias:["Ingles","Tecnologias de internet","Base de datos"]
},
{
    matrucula:"2020030331",
    nombre:"Lopez Gonzales Erick Jeanick",
    sexo:"M",
    materias:["Ingles","Tecnologias de internet","Base de datos"]
},
]

app.get("/tablasC",(req,res)=>{
    const valores ={
        tablas:req.query.tablas
    }
    res.render('tablasC',valores);
})


app.post("/tablasC",(req,res)=>{
    const valores ={
        tablas:req.body.tablas
    }
    res.render('tablasC',valores);
})

app.use(express.urlencoded({ extended: true }));
app.set('view engine', 'ejs');
//GET
app.get('/cotizacionc', (req, res) => {
    const val ={
        valor:req.query.valor,
        pInicial:req.query.pInicial,
        plazos:req.query.plazos
    }
    res.render('cotizacionC',val);
});

//POST
app.post('/cotizacionC', (req, res) => {
    const val ={
        valor:req.body.valor,
        pInicial:req.body.pInicial,
        plazos:req.body.plazos
    }
    res.render('cotizacionC',val);
});





app.get('/',(req,res)=>{
    //res.send('Iniciamos con express')
    res.render('index1',{titulo:'Listado de alumnos',listado:datos})
})

app.use((req,res,next)=>{
    res.status(404).sendFile(__dirname + '/public/error.html');

})

app.listen(port,()=>{
    console.log('Inciado el puerto 3001')
})


